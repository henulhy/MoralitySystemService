package cn.stylefeng.guns.modular.actor.service;

import cn.stylefeng.guns.modular.system.model.SysActor;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lhy
 * @since 2019-10-06
 */
public interface ISysActorService extends IService<SysActor> {

}
