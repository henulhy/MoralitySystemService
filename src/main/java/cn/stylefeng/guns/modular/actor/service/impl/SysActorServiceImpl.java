package cn.stylefeng.guns.modular.actor.service.impl;

import cn.stylefeng.guns.modular.system.model.SysActor;
import cn.stylefeng.guns.modular.system.dao.SysActorMapper;
import cn.stylefeng.guns.modular.actor.service.ISysActorService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lhy
 * @since 2019-10-06
 */
@Service
public class SysActorServiceImpl extends ServiceImpl<SysActorMapper, SysActor> implements ISysActorService {

}
