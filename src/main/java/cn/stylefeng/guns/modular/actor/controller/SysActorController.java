package cn.stylefeng.guns.modular.actor.controller;

import cn.stylefeng.guns.modular.actor.service.impl.SysActorServiceImpl;
import cn.stylefeng.roses.core.base.controller.BaseController;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.guns.core.log.LogObjectHolder;
import cn.stylefeng.guns.modular.system.model.SysActor;
import cn.stylefeng.guns.modular.actor.service.ISysActorService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 测试者控制器
 *
 * @author fengshuonan
 * @Date 2019-10-06 22:37:57
 */
@Controller
@RequestMapping("api/sysActor")
public class SysActorController extends BaseController {

    private String PREFIX = "/actor/sysActor/";

    @Autowired
    private ISysActorService sysActorService;

    /**
     * 跳转到测试者首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sysActor.html";
    }

    /**
     * 跳转到添加测试者
     */
    @RequestMapping("/sysActor_add")
    public String sysActorAdd() {
        return PREFIX + "sysActor_add.html";
    }

    /**
     * 跳转到修改测试者
     */
    @RequestMapping("/sysActor_update/{sysActorId}")
    public String sysActorUpdate(@PathVariable Integer sysActorId, Model model) {
        SysActor sysActor = sysActorService.selectById(sysActorId);
        model.addAttribute("item",sysActor);
        LogObjectHolder.me().set(sysActor);
        return PREFIX + "sysActor_edit.html";
    }

    /**
     * 获取测试者列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sysActorService.selectList(null);
    }

    /**
     * 新增测试者
     */
    @PostMapping(value = "/add")
    @ResponseBody
    public Object add(@RequestBody  SysActor sysActor) {
        System.out.println("############################:  "+sysActor.toString());
        sysActorService.insert(sysActor);
        return SUCCESS_TIP;
    }

    /**
     * 删除测试者
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sysActorId) {
        sysActorService.deleteById(sysActorId);
        return SUCCESS_TIP;
    }

    /**
     * 修改测试者
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SysActor sysActor) {
        sysActorService.updateById(sysActor);
        return SUCCESS_TIP;
    }

    /**
     * 测试者详情
     */
    @RequestMapping(value = "/detail/{sysActorId}")
    @ResponseBody
    public Object detail(@PathVariable("sysActorId") Integer sysActorId) {
        return sysActorService.selectById(sysActorId);
    }

    /**
     * 测试者详情
     */
    @RequestMapping(value = "/login")
    @ResponseBody
    public Object judgeLogin(@RequestParam(name = "username",required = true) String username,@RequestParam(name = "credential",required = true) String password) {

        Map<String,Object> map = new HashMap<>();
        map.put("account",username.trim());
        map.put("password",password.trim());
        List<SysActor> actors = sysActorService.selectByMap(map);
        if(actors.size() > 0 ){
            return true;
        }else{
            return false;
        }
    }
}
