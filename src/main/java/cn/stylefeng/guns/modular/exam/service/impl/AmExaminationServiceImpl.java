package cn.stylefeng.guns.modular.exam.service.impl;

import cn.stylefeng.guns.modular.system.model.AmExamination;
import cn.stylefeng.guns.modular.system.dao.AmExaminationMapper;
import cn.stylefeng.guns.modular.exam.service.IAmExaminationService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
@Service
public class AmExaminationServiceImpl extends ServiceImpl<AmExaminationMapper, AmExamination> implements IAmExaminationService {

}
