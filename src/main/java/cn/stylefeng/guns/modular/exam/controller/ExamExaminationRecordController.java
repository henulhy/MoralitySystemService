package cn.stylefeng.guns.modular.exam.controller;

import cn.stylefeng.guns.modular.exam.service.IExamExaminationRecordService;
import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.guns.modular.system.model.ExamExaminationRecord;

/**
 * 考试测验控制器
 *
 * @author fengshuonan
 * @Date 2019-10-16 20:44:10
 */
@Controller
@RequestMapping("/examExaminationRecord")
public class ExamExaminationRecordController extends BaseController {

    private String PREFIX = "/exam/examExaminationRecord/";

    @Autowired
    private IExamExaminationRecordService examExaminationRecordService;

    /**
     * 跳转到考试测验首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "examExaminationRecord.html";
    }

    /**
     * 跳转到添加考试测验
     */
    @RequestMapping("/examExaminationRecord_add")
    public String examExaminationRecordAdd() {
        return PREFIX + "examExaminationRecord_add.html";
    }

    /**
     * 跳转到修改考试测验
     */
    @RequestMapping("/examExaminationRecord_update/{examExaminationRecordId}")
    public String examExaminationRecordUpdate(@PathVariable Integer examExaminationRecordId, Model model) {
        ExamExaminationRecord examExaminationRecord = examExaminationRecordService.selectById(examExaminationRecordId);
        model.addAttribute("item",examExaminationRecord);
        LogObjectHolder.me().set(examExaminationRecord);
        return PREFIX + "examExaminationRecord_edit.html";
    }

    /**
     * 获取考试测验列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return examExaminationRecordService.selectList(null);
    }

    /**
     * 新增考试测验
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(ExamExaminationRecord examExaminationRecord) {
        examExaminationRecordService.insert(examExaminationRecord);
        return SUCCESS_TIP;
    }

    /**
     * 删除考试测验
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer examExaminationRecordId) {
        examExaminationRecordService.deleteById(examExaminationRecordId);
        return SUCCESS_TIP;
    }

    /**
     * 修改考试测验
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(ExamExaminationRecord examExaminationRecord) {
        examExaminationRecordService.updateById(examExaminationRecord);
        return SUCCESS_TIP;
    }

    /**
     * 考试测验详情
     */
    @RequestMapping(value = "/detail/{examExaminationRecordId}")
    @ResponseBody
    public Object detail(@PathVariable("examExaminationRecordId") Integer examExaminationRecordId) {
        return examExaminationRecordService.selectById(examExaminationRecordId);
    }
}
