package cn.stylefeng.guns.modular.exam.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.guns.modular.system.model.AmAnswer;
import cn.stylefeng.guns.modular.exam.service.IAmAnswerService;

/**
 * 考试测验控制器
 *
 * @author fengshuonan
 * @Date 2019-10-16 12:54:41
 */
@Controller
@RequestMapping("/amAnswer")
public class AmAnswerController extends BaseController {

    private String PREFIX = "/exam/amAnswer/";

    @Autowired
    private IAmAnswerService amAnswerService;

    /**
     * 跳转到考试测验首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "amAnswer.html";
    }

    /**
     * 跳转到添加考试测验
     */
    @RequestMapping("/amAnswer_add")
    public String amAnswerAdd() {
        return PREFIX + "amAnswer_add.html";
    }

    /**
     * 跳转到修改考试测验
     */
    @RequestMapping("/amAnswer_update/{amAnswerId}")
    public String amAnswerUpdate(@PathVariable Integer amAnswerId, Model model) {
        AmAnswer amAnswer = amAnswerService.selectById(amAnswerId);
        model.addAttribute("item",amAnswer);
        LogObjectHolder.me().set(amAnswer);
        return PREFIX + "amAnswer_edit.html";
    }

    /**
     * 获取考试测验列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return amAnswerService.selectList(null);
    }

    /**
     * 新增考试测验
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AmAnswer amAnswer) {
        amAnswerService.insert(amAnswer);
        return SUCCESS_TIP;
    }

    /**
     * 删除考试测验
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer amAnswerId) {
        amAnswerService.deleteById(amAnswerId);
        return SUCCESS_TIP;
    }

    /**
     * 修改考试测验
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AmAnswer amAnswer) {
        amAnswerService.updateById(amAnswer);
        return SUCCESS_TIP;
    }

    /**
     * 考试测验详情
     */
    @RequestMapping(value = "/detail/{amAnswerId}")
    @ResponseBody
    public Object detail(@PathVariable("amAnswerId") Integer amAnswerId) {
        return amAnswerService.selectById(amAnswerId);
    }
}
