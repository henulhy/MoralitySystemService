package cn.stylefeng.guns.modular.exam.service.impl;

import cn.stylefeng.guns.modular.exam.service.IAmAnswerService;
import cn.stylefeng.guns.modular.system.model.AmAnswer;
import cn.stylefeng.guns.modular.system.dao.AmAnswerMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
@Service
public class AmAnswerServiceImpl extends ServiceImpl<AmAnswerMapper, AmAnswer> implements IAmAnswerService {

}
