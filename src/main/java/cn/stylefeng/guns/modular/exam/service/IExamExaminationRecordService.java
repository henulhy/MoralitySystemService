package cn.stylefeng.guns.modular.exam.service;

import cn.stylefeng.guns.modular.system.model.ExamExaminationRecord;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
public interface IExamExaminationRecordService extends IService<ExamExaminationRecord> {

}
