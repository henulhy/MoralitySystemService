package cn.stylefeng.guns.modular.exam.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.guns.modular.system.model.AmSubject;
import cn.stylefeng.guns.modular.exam.service.IAmSubjectService;

/**
 * 考试测验控制器
 *
 * @author fengshuonan
 * @Date 2019-10-16 12:54:17
 */
@Controller
@RequestMapping("/amSubject")
public class AmSubjectController extends BaseController {

    private String PREFIX = "/exam/amSubject/";

    @Autowired
    private IAmSubjectService amSubjectService;

    /**
     * 跳转到考试测验首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "amSubject.html";
    }

    /**
     * 跳转到添加考试测验
     */
    @RequestMapping("/amSubject_add")
    public String amSubjectAdd() {
        return PREFIX + "amSubject_add.html";
    }

    /**
     * 跳转到修改考试测验
     */
    @RequestMapping("/amSubject_update/{amSubjectId}")
    public String amSubjectUpdate(@PathVariable Integer amSubjectId, Model model) {
        AmSubject amSubject = amSubjectService.selectById(amSubjectId);
        model.addAttribute("item",amSubject);
        LogObjectHolder.me().set(amSubject);
        return PREFIX + "amSubject_edit.html";
    }

    /**
     * 获取考试测验列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return amSubjectService.selectList(null);
    }

    /**
     * 新增考试测验
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AmSubject amSubject) {
        amSubjectService.insert(amSubject);
        return SUCCESS_TIP;
    }

    /**
     * 删除考试测验
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer amSubjectId) {
        amSubjectService.deleteById(amSubjectId);
        return SUCCESS_TIP;
    }

    /**
     * 修改考试测验
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AmSubject amSubject) {
        amSubjectService.updateById(amSubject);
        return SUCCESS_TIP;
    }

    /**
     * 考试测验详情
     */
    @RequestMapping(value = "/detail/{amSubjectId}")
    @ResponseBody
    public Object detail(@PathVariable("amSubjectId") Integer amSubjectId) {
        return amSubjectService.selectById(amSubjectId);
    }
}
