package cn.stylefeng.guns.modular.exam.service.impl;

import cn.stylefeng.guns.modular.system.model.AmSubject;
import cn.stylefeng.guns.modular.system.dao.AmSubjectMapper;
import cn.stylefeng.guns.modular.exam.service.IAmSubjectService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
@Service
public class AmSubjectServiceImpl extends ServiceImpl<AmSubjectMapper, AmSubject> implements IAmSubjectService {

}
