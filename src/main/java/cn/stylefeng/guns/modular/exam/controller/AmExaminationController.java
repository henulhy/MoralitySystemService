package cn.stylefeng.guns.modular.exam.controller;

import cn.stylefeng.guns.modular.exam.service.IAmExaminationService;
import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.guns.modular.system.model.AmExamination;

/**
 * 考试测验控制器
 *
 * @author fengshuonan
 * @Date 2019-10-16 12:54:28
 */
@Controller
@RequestMapping("/amExamination")
public class AmExaminationController extends BaseController {

    private String PREFIX = "/exam/amExamination/";

    @Autowired
    private IAmExaminationService amExaminationService;

    /**
     * 跳转到考试测验首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "amExamination.html";
    }

    /**
     * 跳转到添加考试测验
     */
    @RequestMapping("/amExamination_add")
    public String amExaminationAdd() {
        return PREFIX + "amExamination_add.html";
    }

    /**
     * 跳转到修改考试测验
     */
    @RequestMapping("/amExamination_update/{amExaminationId}")
    public String amExaminationUpdate(@PathVariable Integer amExaminationId, Model model) {
        AmExamination amExamination = amExaminationService.selectById(amExaminationId);
        model.addAttribute("item",amExamination);
        LogObjectHolder.me().set(amExamination);
        return PREFIX + "amExamination_edit.html";
    }

    /**
     * 获取考试测验列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return amExaminationService.selectList(null);
    }

    /**
     * 新增考试测验
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AmExamination amExamination) {
        amExaminationService.insert(amExamination);
        return SUCCESS_TIP;
    }

    /**
     * 删除考试测验
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer amExaminationId) {
        amExaminationService.deleteById(amExaminationId);
        return SUCCESS_TIP;
    }

    /**
     * 修改考试测验
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AmExamination amExamination) {
        amExaminationService.updateById(amExamination);
        return SUCCESS_TIP;
    }

    /**
     * 考试测验详情
     */
    @RequestMapping(value = "/detail/{amExaminationId}")
    @ResponseBody
    public Object detail(@PathVariable("amExaminationId") Integer amExaminationId) {
        return amExaminationService.selectById(amExaminationId);
    }
}
