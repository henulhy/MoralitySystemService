package cn.stylefeng.guns.modular.exam.service.impl;

import cn.stylefeng.guns.modular.system.model.ExamExaminationRecord;
import cn.stylefeng.guns.modular.system.dao.ExamExaminationRecordMapper;
import cn.stylefeng.guns.modular.exam.service.IExamExaminationRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
@Service
public class ExamExaminationRecordServiceImpl extends ServiceImpl<ExamExaminationRecordMapper, ExamExaminationRecord> implements IExamExaminationRecordService {

}
