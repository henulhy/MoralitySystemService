package cn.stylefeng.guns.modular.exam.service;

import cn.stylefeng.guns.modular.system.model.AmSubject;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
public interface IAmSubjectService extends IService<AmSubject> {

}
