package cn.stylefeng.guns.modular.exam.service.impl;

import cn.stylefeng.guns.modular.system.model.AmExaminationSubject;
import cn.stylefeng.guns.modular.system.dao.AmExaminationSubjectMapper;
import cn.stylefeng.guns.modular.exam.service.IAmExaminationSubjectService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
@Service
public class AmExaminationSubjectServiceImpl extends ServiceImpl<AmExaminationSubjectMapper, AmExaminationSubject> implements IAmExaminationSubjectService {

}
