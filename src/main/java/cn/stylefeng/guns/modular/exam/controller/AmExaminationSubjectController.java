package cn.stylefeng.guns.modular.exam.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.guns.modular.system.model.AmExaminationSubject;
import cn.stylefeng.guns.modular.exam.service.IAmExaminationSubjectService;

/**
 * 考试测验控制器
 *
 * @author fengshuonan
 * @Date 2019-10-16 12:55:00
 */
@Controller
@RequestMapping("/amExaminationSubject")
public class AmExaminationSubjectController extends BaseController {

    private String PREFIX = "/exam/amExaminationSubject/";

    @Autowired
    private IAmExaminationSubjectService amExaminationSubjectService;

    /**
     * 跳转到考试测验首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "amExaminationSubject.html";
    }

    /**
     * 跳转到添加考试测验
     */
    @RequestMapping("/amExaminationSubject_add")
    public String amExaminationSubjectAdd() {
        return PREFIX + "amExaminationSubject_add.html";
    }

    /**
     * 跳转到修改考试测验
     */
    @RequestMapping("/amExaminationSubject_update/{amExaminationSubjectId}")
    public String amExaminationSubjectUpdate(@PathVariable Integer amExaminationSubjectId, Model model) {
        AmExaminationSubject amExaminationSubject = amExaminationSubjectService.selectById(amExaminationSubjectId);
        model.addAttribute("item",amExaminationSubject);
        LogObjectHolder.me().set(amExaminationSubject);
        return PREFIX + "amExaminationSubject_edit.html";
    }

    /**
     * 获取考试测验列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return amExaminationSubjectService.selectList(null);
    }

    /**
     * 新增考试测验
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AmExaminationSubject amExaminationSubject) {
        amExaminationSubjectService.insert(amExaminationSubject);
        return SUCCESS_TIP;
    }

    /**
     * 删除考试测验
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer amExaminationSubjectId) {
        amExaminationSubjectService.deleteById(amExaminationSubjectId);
        return SUCCESS_TIP;
    }

    /**
     * 修改考试测验
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AmExaminationSubject amExaminationSubject) {
        amExaminationSubjectService.updateById(amExaminationSubject);
        return SUCCESS_TIP;
    }

    /**
     * 考试测验详情
     */
    @RequestMapping(value = "/detail/{amExaminationSubjectId}")
    @ResponseBody
    public Object detail(@PathVariable("amExaminationSubjectId") Integer amExaminationSubjectId) {
        return amExaminationSubjectService.selectById(amExaminationSubjectId);
    }
}
