package cn.stylefeng.guns.modular.system.dao;

import cn.stylefeng.guns.modular.system.model.AmAnswer;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
public interface AmAnswerMapper extends BaseMapper<AmAnswer> {

}
