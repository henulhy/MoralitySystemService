package cn.stylefeng.guns.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
@TableName("exam_subject")
public class AmSubject extends Model<AmSubject> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 题干
     */
    @TableField("subject_name")
    private String subjectName;
    /**
     * 题目类型0-单选择 1-多选择 3-填空
     */
    @TableField("subject_type")
    private Integer subjectType;
    /**
     * 题目内容如选项
     */
    @TableField("subject_src")
    private String subjectSrc;
    /**
     * 参考答案
     */
    @TableField("subject_answer")
    private String subjectAnswer;
    /**
     * 题目分值
     */
    @TableField("subject_score")
    private Date subjectScore;
    /**
     * 题目解析
     */
    @TableField("subject_analysis")
    private String subjectAnalysis;
    /**
     * 题目难度 1 - 10
     */
    @TableField("subject_level")
    private Integer subjectLevel;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 创建人
     */
    @TableField("create_name")
    private String createName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Integer getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(Integer subjectType) {
        this.subjectType = subjectType;
    }

    public String getSubjectSrc() {
        return subjectSrc;
    }

    public void setSubjectSrc(String subjectSrc) {
        this.subjectSrc = subjectSrc;
    }

    public String getSubjectAnswer() {
        return subjectAnswer;
    }

    public void setSubjectAnswer(String subjectAnswer) {
        this.subjectAnswer = subjectAnswer;
    }

    public Date getSubjectScore() {
        return subjectScore;
    }

    public void setSubjectScore(Date subjectScore) {
        this.subjectScore = subjectScore;
    }

    public String getSubjectAnalysis() {
        return subjectAnalysis;
    }

    public void setSubjectAnalysis(String subjectAnalysis) {
        this.subjectAnalysis = subjectAnalysis;
    }

    public Integer getSubjectLevel() {
        return subjectLevel;
    }

    public void setSubjectLevel(Integer subjectLevel) {
        this.subjectLevel = subjectLevel;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AmSubject{" +
        ", id=" + id +
        ", subjectName=" + subjectName +
        ", subjectType=" + subjectType +
        ", subjectSrc=" + subjectSrc +
        ", subjectAnswer=" + subjectAnswer +
        ", subjectScore=" + subjectScore +
        ", subjectAnalysis=" + subjectAnalysis +
        ", subjectLevel=" + subjectLevel +
        ", createTime=" + createTime +
        ", createName=" + createName +
        "}";
    }
}
