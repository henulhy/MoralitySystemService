package cn.stylefeng.guns.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
@TableName("exam_answer")
public class AmAnswer extends Model<AmAnswer> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 考试记录id
     */
    @TableField("record_id")
    private Integer recordId;
    /**
     * 题目id
     */
    @TableField("subject_id")
    private Integer subjectId;
    /**
     * 用户答案
     */
    @TableField("user_answer")
    private String userAnswer;
    /**
     * 答案类型 1 - 正确 0 - 错误
     */
    @TableField("answer_type")
    private Integer answerType;
    /**
     * 单题得分
     */
    @TableField("subject_score")
    private Integer subjectScore;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 创建人
     */
    @TableField("create_name")
    private String createName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public Integer getAnswerType() {
        return answerType;
    }

    public void setAnswerType(Integer answerType) {
        this.answerType = answerType;
    }

    public Integer getSubjectScore() {
        return subjectScore;
    }

    public void setSubjectScore(Integer subjectScore) {
        this.subjectScore = subjectScore;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AmAnswer{" +
        ", id=" + id +
        ", recordId=" + recordId +
        ", subjectId=" + subjectId +
        ", userAnswer=" + userAnswer +
        ", answerType=" + answerType +
        ", subjectScore=" + subjectScore +
        ", createTime=" + createTime +
        ", createName=" + createName +
        "}";
    }
}
