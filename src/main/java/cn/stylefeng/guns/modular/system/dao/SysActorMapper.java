package cn.stylefeng.guns.modular.system.dao;

import cn.stylefeng.guns.modular.system.model.SysActor;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lhy
 * @since 2019-10-06
 */
public interface SysActorMapper extends BaseMapper<SysActor> {

}
