package cn.stylefeng.guns.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
@TableName("exam_examination_record")
public class ExamExaminationRecord extends Model<ExamExaminationRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 考试id
     */
    @TableField("examinnation_id")
    private Integer examinnationId;
    /**
     * 开始时间
     */
    @TableField("start_time")
    private Date startTime;
    /**
     * 结束时间
     */
    @TableField("end_time")
    private Date endTime;
    /**
     * 所的总分
     */
    private Integer score;
    /**
     * 正确个数
     */
    @TableField("correct_nub")
    private Integer correctNub;
    /**
     * 不正确个数
     */
    @TableField("incorrect_nub")
    private Integer incorrectNub;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 创建人
     */
    @TableField("create_name")
    private String createName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getExaminnationId() {
        return examinnationId;
    }

    public void setExaminnationId(Integer examinnationId) {
        this.examinnationId = examinnationId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getCorrectNub() {
        return correctNub;
    }

    public void setCorrectNub(Integer correctNub) {
        this.correctNub = correctNub;
    }

    public Integer getIncorrectNub() {
        return incorrectNub;
    }

    public void setIncorrectNub(Integer incorrectNub) {
        this.incorrectNub = incorrectNub;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ExamExaminationRecord{" +
        ", id=" + id +
        ", userId=" + userId +
        ", examinnationId=" + examinnationId +
        ", startTime=" + startTime +
        ", endTime=" + endTime +
        ", score=" + score +
        ", correctNub=" + correctNub +
        ", incorrectNub=" + incorrectNub +
        ", createTime=" + createTime +
        ", createName=" + createName +
        "}";
    }
}
