package cn.stylefeng.guns.modular.show.controller;

import cn.stylefeng.guns.modular.show.service.ILawsService;
import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.guns.modular.system.model.Laws;

/**
 * 新闻显示控制器
 *
 * @author fengshuonan
 * @Date 2019-10-16 12:53:18
 */
@Controller
@RequestMapping("/laws")
public class LawsController extends BaseController {

    private String PREFIX = "/show/laws/";

    @Autowired
    private ILawsService lawsService;

    /**
     * 跳转到新闻显示首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "laws.html";
    }

    /**
     * 跳转到添加新闻显示
     */
    @RequestMapping("/laws_add")
    public String lawsAdd() {
        return PREFIX + "laws_add.html";
    }

    /**
     * 跳转到修改新闻显示
     */
    @RequestMapping("/laws_update/{lawsId}")
    public String lawsUpdate(@PathVariable Integer lawsId, Model model) {
        Laws laws = lawsService.selectById(lawsId);
        model.addAttribute("item",laws);
        LogObjectHolder.me().set(laws);
        return PREFIX + "laws_edit.html";
    }

    /**
     * 获取新闻显示列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return lawsService.selectList(null);
    }

    /**
     * 新增新闻显示
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Laws laws) {
        lawsService.insert(laws);
        return SUCCESS_TIP;
    }

    /**
     * 删除新闻显示
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer lawsId) {
        lawsService.deleteById(lawsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改新闻显示
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Laws laws) {
        lawsService.updateById(laws);
        return SUCCESS_TIP;
    }

    /**
     * 新闻显示详情
     */
    @RequestMapping(value = "/detail/{lawsId}")
    @ResponseBody
    public Object detail(@PathVariable("lawsId") Integer lawsId) {
        return lawsService.selectById(lawsId);
    }
}
