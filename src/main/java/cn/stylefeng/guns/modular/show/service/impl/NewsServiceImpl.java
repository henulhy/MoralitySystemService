package cn.stylefeng.guns.modular.show.service.impl;

import cn.stylefeng.guns.modular.show.service.INewsService;
import cn.stylefeng.guns.modular.system.model.News;
import cn.stylefeng.guns.modular.system.dao.NewsMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
@Service
public class NewsServiceImpl extends ServiceImpl<NewsMapper, News> implements INewsService {

}
