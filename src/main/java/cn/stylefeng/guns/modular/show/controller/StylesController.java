package cn.stylefeng.guns.modular.show.controller;

import cn.stylefeng.guns.modular.show.service.IStylesService;
import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.guns.modular.system.model.Styles;

/**
 * 新闻显示控制器
 *
 * @author fengshuonan
 * @Date 2019-10-16 12:53:44
 */
@Controller
@RequestMapping("/styles")
public class StylesController extends BaseController {

    private String PREFIX = "/show/styles/";

    @Autowired
    private IStylesService stylesService;

    /**
     * 跳转到新闻显示首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "styles.html";
    }

    /**
     * 跳转到添加新闻显示
     */
    @RequestMapping("/styles_add")
    public String stylesAdd() {
        return PREFIX + "styles_add.html";
    }

    /**
     * 跳转到修改新闻显示
     */
    @RequestMapping("/styles_update/{stylesId}")
    public String stylesUpdate(@PathVariable Integer stylesId, Model model) {
        Styles styles = stylesService.selectById(stylesId);
        model.addAttribute("item",styles);
        LogObjectHolder.me().set(styles);
        return PREFIX + "styles_edit.html";
    }

    /**
     * 获取新闻显示列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return stylesService.selectList(null);
    }

    /**
     * 新增新闻显示
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Styles styles) {
        stylesService.insert(styles);
        return SUCCESS_TIP;
    }

    /**
     * 删除新闻显示
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer stylesId) {
        stylesService.deleteById(stylesId);
        return SUCCESS_TIP;
    }

    /**
     * 修改新闻显示
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Styles styles) {
        stylesService.updateById(styles);
        return SUCCESS_TIP;
    }

    /**
     * 新闻显示详情
     */
    @RequestMapping(value = "/detail/{stylesId}")
    @ResponseBody
    public Object detail(@PathVariable("stylesId") Integer stylesId) {
        return stylesService.selectById(stylesId);
    }
}
