package cn.stylefeng.guns.modular.show.controller;

import cn.stylefeng.guns.modular.show.service.IRadioService;
import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import cn.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import cn.stylefeng.guns.modular.system.model.Radio;

/**
 * 新闻显示控制器
 *
 * @author fengshuonan
 * @Date 2019-10-16 12:53:37
 */
@Controller
@RequestMapping("/radio")
public class RadioController extends BaseController {

    private String PREFIX = "/show/radio/";

    @Autowired
    private IRadioService radioService;

    /**
     * 跳转到新闻显示首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "radio.html";
    }

    /**
     * 跳转到添加新闻显示
     */
    @RequestMapping("/radio_add")
    public String radioAdd() {
        return PREFIX + "radio_add.html";
    }

    /**
     * 跳转到修改新闻显示
     */
    @RequestMapping("/radio_update/{radioId}")
    public String radioUpdate(@PathVariable Integer radioId, Model model) {
        Radio radio = radioService.selectById(radioId);
        model.addAttribute("item",radio);
        LogObjectHolder.me().set(radio);
        return PREFIX + "radio_edit.html";
    }

    /**
     * 获取新闻显示列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return radioService.selectList(null);
    }

    /**
     * 新增新闻显示
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Radio radio) {
        radioService.insert(radio);
        return SUCCESS_TIP;
    }

    /**
     * 删除新闻显示
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer radioId) {
        radioService.deleteById(radioId);
        return SUCCESS_TIP;
    }

    /**
     * 修改新闻显示
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Radio radio) {
        radioService.updateById(radio);
        return SUCCESS_TIP;
    }

    /**
     * 新闻显示详情
     */
    @RequestMapping(value = "/detail/{radioId}")
    @ResponseBody
    public Object detail(@PathVariable("radioId") Integer radioId) {
        return radioService.selectById(radioId);
    }
}
