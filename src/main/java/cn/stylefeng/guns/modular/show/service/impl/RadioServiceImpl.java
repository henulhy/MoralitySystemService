package cn.stylefeng.guns.modular.show.service.impl;

import cn.stylefeng.guns.modular.show.service.IRadioService;
import cn.stylefeng.guns.modular.system.model.Radio;
import cn.stylefeng.guns.modular.system.dao.RadioMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
@Service
public class RadioServiceImpl extends ServiceImpl<RadioMapper, Radio> implements IRadioService {

}
