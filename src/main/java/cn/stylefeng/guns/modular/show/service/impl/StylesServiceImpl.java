package cn.stylefeng.guns.modular.show.service.impl;

import cn.stylefeng.guns.modular.show.service.IStylesService;
import cn.stylefeng.guns.modular.system.model.Styles;
import cn.stylefeng.guns.modular.system.dao.StylesMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
@Service
public class StylesServiceImpl extends ServiceImpl<StylesMapper, Styles> implements IStylesService {

}
