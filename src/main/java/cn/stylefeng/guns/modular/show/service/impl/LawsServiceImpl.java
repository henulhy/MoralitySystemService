package cn.stylefeng.guns.modular.show.service.impl;

import cn.stylefeng.guns.modular.show.service.ILawsService;
import cn.stylefeng.guns.modular.system.model.Laws;
import cn.stylefeng.guns.modular.system.dao.LawsMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
@Service
public class LawsServiceImpl extends ServiceImpl<LawsMapper, Laws> implements ILawsService {

}
