package cn.stylefeng.guns.modular.show.service;

import cn.stylefeng.guns.modular.system.model.Radio;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lhy
 * @since 2019-10-16
 */
public interface IRadioService extends IService<Radio> {

}
