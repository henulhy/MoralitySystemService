/**
 * 测试者管理初始化
 */
var SysActor = {
    id: "SysActorTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SysActor.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'age', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'account', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'password', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SysActor.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SysActor.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加测试者
 */
SysActor.openAddSysActor = function () {
    var index = layer.open({
        type: 2,
        title: '添加测试者',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sysActor/sysActor_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看测试者详情
 */
SysActor.openSysActorDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '测试者详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sysActor/sysActor_update/' + SysActor.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除测试者
 */
SysActor.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sysActor/delete", function (data) {
            Feng.success("删除成功!");
            SysActor.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sysActorId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询测试者列表
 */
SysActor.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SysActor.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SysActor.initColumn();
    var table = new BSTable(SysActor.id, "/sysActor/list", defaultColunms);
    table.setPaginationType("client");
    SysActor.table = table.init();
});
