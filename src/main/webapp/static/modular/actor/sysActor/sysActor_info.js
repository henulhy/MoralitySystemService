/**
 * 初始化测试者详情对话框
 */
var SysActorInfoDlg = {
    sysActorInfoData : {}
};

/**
 * 清除数据
 */
SysActorInfoDlg.clearData = function() {
    this.sysActorInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SysActorInfoDlg.set = function(key, val) {
    this.sysActorInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SysActorInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SysActorInfoDlg.close = function() {
    parent.layer.close(window.parent.SysActor.layerIndex);
}

/**
 * 收集数据
 */
SysActorInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('age')
    .set('account')
    .set('password');
}

/**
 * 提交添加
 */
SysActorInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sysActor/add", function(data){
        Feng.success("添加成功!");
        window.parent.SysActor.table.refresh();
        SysActorInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sysActorInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SysActorInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sysActor/update", function(data){
        Feng.success("修改成功!");
        window.parent.SysActor.table.refresh();
        SysActorInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sysActorInfoData);
    ajax.start();
}

$(function() {

});
