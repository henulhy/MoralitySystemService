/**
 * 新闻显示管理初始化
 */
var Laws = {
    id: "LawsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Laws.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '题目', field: 'title', visible: true, align: 'center', valign: 'middle'},
            {title: '文章内容', field: 'src', visible: true, align: 'center', valign: 'middle'},
            {title: '发布标记', field: 'releaseFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '发布时间', field: 'releaseTime', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '创建人', field: 'createName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Laws.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Laws.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加新闻显示
 */
Laws.openAddLaws = function () {
    var index = layer.open({
        type: 2,
        title: '添加新闻显示',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/laws/laws_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看新闻显示详情
 */
Laws.openLawsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '新闻显示详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/laws/laws_update/' + Laws.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除新闻显示
 */
Laws.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/laws/delete", function (data) {
            Feng.success("删除成功!");
            Laws.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("lawsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询新闻显示列表
 */
Laws.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Laws.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Laws.initColumn();
    var table = new BSTable(Laws.id, "/laws/list", defaultColunms);
    table.setPaginationType("client");
    Laws.table = table.init();
});
