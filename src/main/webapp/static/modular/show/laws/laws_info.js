/**
 * 初始化新闻显示详情对话框
 */
var LawsInfoDlg = {
    lawsInfoData : {}
};

/**
 * 清除数据
 */
LawsInfoDlg.clearData = function() {
    this.lawsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LawsInfoDlg.set = function(key, val) {
    this.lawsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LawsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LawsInfoDlg.close = function() {
    parent.layer.close(window.parent.Laws.layerIndex);
}

/**
 * 收集数据
 */
LawsInfoDlg.collectData = function() {
    this
    .set('id')
    .set('title')
    .set('src')
    .set('releaseFlag')
    .set('releaseTime')
    .set('createTime')
    .set('createName');
}

/**
 * 提交添加
 */
LawsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/laws/add", function(data){
        Feng.success("添加成功!");
        window.parent.Laws.table.refresh();
        LawsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.lawsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LawsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/laws/update", function(data){
        Feng.success("修改成功!");
        window.parent.Laws.table.refresh();
        LawsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.lawsInfoData);
    ajax.start();
}

$(function() {

});
