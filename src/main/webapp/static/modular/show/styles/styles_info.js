/**
 * 初始化新闻显示详情对话框
 */
var StylesInfoDlg = {
    stylesInfoData : {}
};

/**
 * 清除数据
 */
StylesInfoDlg.clearData = function() {
    this.stylesInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
StylesInfoDlg.set = function(key, val) {
    this.stylesInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
StylesInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
StylesInfoDlg.close = function() {
    parent.layer.close(window.parent.Styles.layerIndex);
}

/**
 * 收集数据
 */
StylesInfoDlg.collectData = function() {
    this
    .set('id')
    .set('title')
    .set('src')
    .set('releaseFlag')
    .set('releaseTime')
    .set('createTime')
    .set('createName');
}

/**
 * 提交添加
 */
StylesInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/styles/add", function(data){
        Feng.success("添加成功!");
        window.parent.Styles.table.refresh();
        StylesInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.stylesInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
StylesInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/styles/update", function(data){
        Feng.success("修改成功!");
        window.parent.Styles.table.refresh();
        StylesInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.stylesInfoData);
    ajax.start();
}

$(function() {

});
