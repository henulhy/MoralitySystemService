/**
 * 初始化新闻显示详情对话框
 */
var RadioInfoDlg = {
    radioInfoData : {}
};

/**
 * 清除数据
 */
RadioInfoDlg.clearData = function() {
    this.radioInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RadioInfoDlg.set = function(key, val) {
    this.radioInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RadioInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
RadioInfoDlg.close = function() {
    parent.layer.close(window.parent.Radio.layerIndex);
}

/**
 * 收集数据
 */
RadioInfoDlg.collectData = function() {
    this
    .set('id')
    .set('title')
    .set('src')
    .set('releaseFlag')
    .set('releaseTime')
    .set('createTime')
    .set('createName');
}

/**
 * 提交添加
 */
RadioInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/radio/add", function(data){
        Feng.success("添加成功!");
        window.parent.Radio.table.refresh();
        RadioInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.radioInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
RadioInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/radio/update", function(data){
        Feng.success("修改成功!");
        window.parent.Radio.table.refresh();
        RadioInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.radioInfoData);
    ajax.start();
}

$(function() {

});
