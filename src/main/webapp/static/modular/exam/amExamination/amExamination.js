/**
 * 考试测验管理初始化
 */
var AmExamination = {
    id: "AmExaminationTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AmExamination.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '考试名称', field: 'examinationName', visible: true, align: 'center', valign: 'middle'},
            {title: '注意事项', field: 'examinationNotice', visible: true, align: 'center', valign: 'middle'},
            {title: '开始时间', field: 'startTime', visible: true, align: 'center', valign: 'middle'},
            {title: '结束时间', field: 'endTime', visible: true, align: 'center', valign: 'middle'},
            {title: '总分', field: 'totalScore', visible: true, align: 'center', valign: 'middle'},
            {title: '状态0-未发布  1-发布', field: 'status', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '创建人', field: 'createName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
AmExamination.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AmExamination.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加考试测验
 */
AmExamination.openAddAmExamination = function () {
    var index = layer.open({
        type: 2,
        title: '添加考试测验',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/amExamination/amExamination_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看考试测验详情
 */
AmExamination.openAmExaminationDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '考试测验详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/amExamination/amExamination_update/' + AmExamination.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除考试测验
 */
AmExamination.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/amExamination/delete", function (data) {
            Feng.success("删除成功!");
            AmExamination.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("amExaminationId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询考试测验列表
 */
AmExamination.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    AmExamination.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AmExamination.initColumn();
    var table = new BSTable(AmExamination.id, "/amExamination/list", defaultColunms);
    table.setPaginationType("client");
    AmExamination.table = table.init();
});
