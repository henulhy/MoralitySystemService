/**
 * 考试测验管理初始化
 */
var AmAnswer = {
    id: "AmAnswerTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AmAnswer.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '考试记录id', field: 'recordId', visible: true, align: 'center', valign: 'middle'},
            {title: '题目id', field: 'subjectId', visible: true, align: 'center', valign: 'middle'},
            {title: '用户答案', field: 'userAnswer', visible: true, align: 'center', valign: 'middle'},
            {title: '答案类型 1 - 正确 0 - 错误', field: 'answerType', visible: true, align: 'center', valign: 'middle'},
            {title: '单题得分', field: 'subjectScore', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '创建人', field: 'createName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
AmAnswer.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AmAnswer.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加考试测验
 */
AmAnswer.openAddAmAnswer = function () {
    var index = layer.open({
        type: 2,
        title: '添加考试测验',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/amAnswer/amAnswer_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看考试测验详情
 */
AmAnswer.openAmAnswerDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '考试测验详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/amAnswer/amAnswer_update/' + AmAnswer.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除考试测验
 */
AmAnswer.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/amAnswer/delete", function (data) {
            Feng.success("删除成功!");
            AmAnswer.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("amAnswerId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询考试测验列表
 */
AmAnswer.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    AmAnswer.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AmAnswer.initColumn();
    var table = new BSTable(AmAnswer.id, "/amAnswer/list", defaultColunms);
    table.setPaginationType("client");
    AmAnswer.table = table.init();
});
