/**
 * 初始化考试测验详情对话框
 */
var AmAnswerInfoDlg = {
    amAnswerInfoData : {}
};

/**
 * 清除数据
 */
AmAnswerInfoDlg.clearData = function() {
    this.amAnswerInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AmAnswerInfoDlg.set = function(key, val) {
    this.amAnswerInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AmAnswerInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AmAnswerInfoDlg.close = function() {
    parent.layer.close(window.parent.AmAnswer.layerIndex);
}

/**
 * 收集数据
 */
AmAnswerInfoDlg.collectData = function() {
    this
    .set('id')
    .set('recordId')
    .set('subjectId')
    .set('userAnswer')
    .set('answerType')
    .set('subjectScore')
    .set('createTime')
    .set('createName');
}

/**
 * 提交添加
 */
AmAnswerInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/amAnswer/add", function(data){
        Feng.success("添加成功!");
        window.parent.AmAnswer.table.refresh();
        AmAnswerInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.amAnswerInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AmAnswerInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/amAnswer/update", function(data){
        Feng.success("修改成功!");
        window.parent.AmAnswer.table.refresh();
        AmAnswerInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.amAnswerInfoData);
    ajax.start();
}

$(function() {

});
