/**
 * 考试测验管理初始化
 */
var AmSubject = {
    id: "AmSubjectTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AmSubject.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '题干', field: 'subjectName', visible: true, align: 'center', valign: 'middle'},
            {title: '题目类型0-单选择 1-多选择 3-填空', field: 'subjectType', visible: true, align: 'center', valign: 'middle'},
            {title: '题目内容如选项', field: 'subjectSrc', visible: true, align: 'center', valign: 'middle'},
            {title: '参考答案', field: 'subjectAnswer', visible: true, align: 'center', valign: 'middle'},
            {title: '题目分值', field: 'subjectScore', visible: true, align: 'center', valign: 'middle'},
            {title: '题目解析', field: 'subjectAnalysis', visible: true, align: 'center', valign: 'middle'},
            {title: '题目难度 1 - 10', field: 'subjectLevel', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '创建人', field: 'createName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
AmSubject.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AmSubject.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加考试测验
 */
AmSubject.openAddAmSubject = function () {
    var index = layer.open({
        type: 2,
        title: '添加考试测验',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/amSubject/amSubject_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看考试测验详情
 */
AmSubject.openAmSubjectDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '考试测验详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/amSubject/amSubject_update/' + AmSubject.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除考试测验
 */
AmSubject.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/amSubject/delete", function (data) {
            Feng.success("删除成功!");
            AmSubject.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("amSubjectId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询考试测验列表
 */
AmSubject.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    AmSubject.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AmSubject.initColumn();
    var table = new BSTable(AmSubject.id, "/amSubject/list", defaultColunms);
    table.setPaginationType("client");
    AmSubject.table = table.init();
});
