/**
 * 初始化考试测验详情对话框
 */
var AmSubjectInfoDlg = {
    amSubjectInfoData : {}
};

/**
 * 清除数据
 */
AmSubjectInfoDlg.clearData = function() {
    this.amSubjectInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AmSubjectInfoDlg.set = function(key, val) {
    this.amSubjectInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AmSubjectInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AmSubjectInfoDlg.close = function() {
    parent.layer.close(window.parent.AmSubject.layerIndex);
}

/**
 * 收集数据
 */
AmSubjectInfoDlg.collectData = function() {
    this
    .set('id')
    .set('subjectName')
    .set('subjectType')
    .set('subjectSrc')
    .set('subjectAnswer')
    .set('subjectScore')
    .set('subjectAnalysis')
    .set('subjectLevel')
    .set('createTime')
    .set('createName');
}

/**
 * 提交添加
 */
AmSubjectInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/amSubject/add", function(data){
        Feng.success("添加成功!");
        window.parent.AmSubject.table.refresh();
        AmSubjectInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.amSubjectInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AmSubjectInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/amSubject/update", function(data){
        Feng.success("修改成功!");
        window.parent.AmSubject.table.refresh();
        AmSubjectInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.amSubjectInfoData);
    ajax.start();
}

$(function() {

});
