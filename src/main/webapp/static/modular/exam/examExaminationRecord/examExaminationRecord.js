/**
 * 考试测验管理初始化
 */
var ExamExaminationRecord = {
    id: "ExamExaminationRecordTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
ExamExaminationRecord.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '用户id', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '考试id', field: 'examinnationId', visible: true, align: 'center', valign: 'middle'},
            {title: '开始时间', field: 'startTime', visible: true, align: 'center', valign: 'middle'},
            {title: '结束时间', field: 'endTime', visible: true, align: 'center', valign: 'middle'},
            {title: '所的总分', field: 'score', visible: true, align: 'center', valign: 'middle'},
            {title: '正确个数', field: 'correctNub', visible: true, align: 'center', valign: 'middle'},
            {title: '不正确个数', field: 'incorrectNub', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '创建人', field: 'createName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
ExamExaminationRecord.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        ExamExaminationRecord.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加考试测验
 */
ExamExaminationRecord.openAddExamExaminationRecord = function () {
    var index = layer.open({
        type: 2,
        title: '添加考试测验',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/examExaminationRecord/examExaminationRecord_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看考试测验详情
 */
ExamExaminationRecord.openExamExaminationRecordDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '考试测验详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/examExaminationRecord/examExaminationRecord_update/' + ExamExaminationRecord.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除考试测验
 */
ExamExaminationRecord.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/examExaminationRecord/delete", function (data) {
            Feng.success("删除成功!");
            ExamExaminationRecord.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("examExaminationRecordId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询考试测验列表
 */
ExamExaminationRecord.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    ExamExaminationRecord.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = ExamExaminationRecord.initColumn();
    var table = new BSTable(ExamExaminationRecord.id, "/examExaminationRecord/list", defaultColunms);
    table.setPaginationType("client");
    ExamExaminationRecord.table = table.init();
});
