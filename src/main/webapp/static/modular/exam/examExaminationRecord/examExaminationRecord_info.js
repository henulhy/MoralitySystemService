/**
 * 初始化考试测验详情对话框
 */
var ExamExaminationRecordInfoDlg = {
    examExaminationRecordInfoData : {}
};

/**
 * 清除数据
 */
ExamExaminationRecordInfoDlg.clearData = function() {
    this.examExaminationRecordInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ExamExaminationRecordInfoDlg.set = function(key, val) {
    this.examExaminationRecordInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ExamExaminationRecordInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ExamExaminationRecordInfoDlg.close = function() {
    parent.layer.close(window.parent.ExamExaminationRecord.layerIndex);
}

/**
 * 收集数据
 */
ExamExaminationRecordInfoDlg.collectData = function() {
    this
    .set('id')
    .set('userId')
    .set('examinnationId')
    .set('startTime')
    .set('endTime')
    .set('score')
    .set('correctNub')
    .set('incorrectNub')
    .set('createTime')
    .set('createName');
}

/**
 * 提交添加
 */
ExamExaminationRecordInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/examExaminationRecord/add", function(data){
        Feng.success("添加成功!");
        window.parent.ExamExaminationRecord.table.refresh();
        ExamExaminationRecordInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.examExaminationRecordInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
ExamExaminationRecordInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/examExaminationRecord/update", function(data){
        Feng.success("修改成功!");
        window.parent.ExamExaminationRecord.table.refresh();
        ExamExaminationRecordInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.examExaminationRecordInfoData);
    ajax.start();
}

$(function() {

});
